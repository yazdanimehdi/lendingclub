import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import itertools
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn import preprocessing
import warnings
import scipy.stats as st
import statsmodels as sm

from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.model_selection import train_test_split

from sklearn.externals.six import StringIO
from IPython.display import Image
from sklearn.tree import export_graphviz
import pydotplus

from sklearn.linear_model import LinearRegression

import statsmodels.api as sm
from scipy import stats


df_loan = pd.read_csv('loan.csv', low_memory=False)

missing_percent = pd.DataFrame(df_loan.isnull().sum()/df_loan.shape[0] *100)
missing_percent = missing_percent.loc[missing_percent[0] > 0].sort_values(by=0, ascending=False)
missing_percent = missing_percent.rename_axis('Columns')
missing_percent.rename(columns={0: 'Percents'})

plt.figure(figsize=(15,5))
g = sns.barplot(missing_percent.index, missing_percent[0])
g.set_xticklabels(missing_percent.index,rotation=90)

plt.figure(figsize=(15,5))
sns.heatmap(df_loan.isnull(), cbar=False, yticklabels=False, cmap="magma")
# dropping missing value columns

df_loan.drop(missing_percent.loc[missing_percent[0] > 70].index, axis=1, inplace=True)
df_loan.drop(['id', 'member_id'], axis=1, inplace=True)

# creating correlation matrix

df_corr = df_loan.drop(['policy_code'], axis=1).corr()

# dropping correlated columns

df_loan.drop(['funded_amnt', 'funded_amnt_inv', 'collection_recovery_fee', 'total_rec_prncp', 'total_pymnt_inv',
              'out_prncp_inv', 'total_rev_hi_lim'], axis=1, inplace=True)

# dropping url and desc

df_loan.drop(['url'], axis=1, inplace=True)

# handling missing values

df_loan['mths_since_last_delinq'] = \
    df_loan['mths_since_last_delinq'].fillna(1.5*df_loan['mths_since_last_delinq'].max())

df_loan.drop(['next_pymnt_d', 'title', 'emp_title'], axis=1, inplace=True)

df_loan["tot_cur_bal"] = df_loan["tot_cur_bal"].fillna(df_loan["tot_cur_bal"].mean())

df_loan["tot_coll_amt"] = df_loan["tot_coll_amt"].fillna(0)

df_loan["emp_length"] = df_loan["emp_length"].fillna(0)

df_loan["revol_util"] = df_loan["revol_util"].fillna(df_loan["revol_util"].mean())

df_loan["collections_12_mths_ex_med"] = \
    df_loan["collections_12_mths_ex_med"].fillna(df_loan["collections_12_mths_ex_med"].mean())

df_loan["delinq_2yrs"] = df_loan["delinq_2yrs"].fillna(0)

df_loan["inq_last_6mths"] = df_loan["inq_last_6mths"].fillna(0)

df_loan["pub_rec"] = df_loan["pub_rec"].fillna(0)

df_loan["acc_now_delinq"] = df_loan["acc_now_delinq"].fillna(0)

df_loan["open_acc"] = df_loan["open_acc"].fillna(df_loan["open_acc"].mean())

df_loan["annual_inc"] = df_loan["annual_inc"].fillna(df_loan["annual_inc"].mean())

df_loan["total_acc"] = df_loan["total_acc"].fillna(df_loan["total_acc"].mean())

df_loan = df_loan.dropna(axis=0)

dt_series = pd.to_datetime(df_loan['issue_d'])
df_loan['year'] = dt_series.dt.year

# Outlier Analysis

i = 0
index_list = df_loan.dtypes.index.tolist()
for item in df_loan.dtypes:
    if str(item) == 'float64':
        plt.cla()
        plt.clf()
        pd.DataFrame(df_loan[index_list[i]].loc[df_loan[index_list[i]] != 0]).boxplot()
        plt.savefig(f'{index_list[i]}.png')
    i += 1

# # Descriptive
#
# # Pie Plots
grades = list()
for item in df_loan['grade']:
    if item in grades:
        continue
    else:
        grades.append(item)

sub_grades = list()
for item in df_loan['sub_grade']:
    if item in sub_grades:
        continue
    else:
        sub_grades.append(item)

totals_grades = list()
for item in grades:
    totals_grades.append(df_loan.loc[df_loan['grade'] == item]['grade'].count())

totals_sub_grades = list()
for item in sub_grades:
    totals_sub_grades.append(df_loan.loc[df_loan['sub_grade'] == item]['sub_grade'].count())

plt.pie(totals_grades, labels=grades)
plt.savefig('pie_grades.png')
plt.clf()
plt.cla()
plt.pie(totals_sub_grades, labels=sub_grades)
plt.savefig('pie_sub_grades.png')
plt.clf()
plt.cla()

plt.figure(figsize=(12, 8))
sns.barplot('year', 'funded_amnt', data=df_loan, palette='tab10')
plt.savefig('issuance.png')


# Histogram
i = 0
index_list = df.dtypes.index.tolist()
for item in df.dtypes:
    if str(item) == 'float64' or str(item) == 'int64':
        plt.cla()
        plt.clf()
        pd.DataFrame(df[index_list[i]]).hist()
        plt.savefig(f'{index_list[i]}_hist.png')
    i += 1
# Fitting Distribution

def best_fit_distribution(data, bins=200, ax=None):
    # Get histogram of original data
    y, x = np.histogram(data, bins=bins, density=True)
    x = (x + np.roll(x, -1))[:-1] / 2.0

    # Distributions to check
    DISTRIBUTIONS = [
        st.alpha,st.anglit,st.arcsine,st.beta,st.betaprime,st.bradford,st.burr,st.cauchy,st.chi,st.chi2,st.cosine,
        st.dgamma,st.dweibull,st.erlang,st.expon,st.exponnorm,st.exponweib,st.exponpow,st.f,st.fatiguelife,st.fisk,
        st.foldcauchy,st.foldnorm,st.frechet_r,st.frechet_l,st.genlogistic,st.genpareto,st.gennorm,st.genexpon,
        st.genextreme,st.gausshyper,st.gamma,st.gengamma,st.genhalflogistic,st.gilbrat,st.gompertz,st.gumbel_r,
        st.gumbel_l,st.halfcauchy,st.halflogistic,st.halfnorm,st.halfgennorm,st.hypsecant,st.invgamma,st.invgauss,
        st.invweibull,st.johnsonsb,st.johnsonsu,st.ksone,st.kstwobign,st.laplace,st.levy,st.levy_l,st.levy_stable,
        st.logistic,st.loggamma,st.loglaplace,st.lognorm,st.lomax,st.maxwell,st.mielke,st.nakagami,st.ncx2,st.ncf,
        st.nct,st.norm,st.pareto,st.pearson3,st.powerlaw,st.powerlognorm,st.powernorm,st.rdist,st.reciprocal,
        st.rayleigh,st.rice,st.recipinvgauss,st.semicircular,st.t,st.triang,st.truncexpon,st.truncnorm,st.tukeylambda,
        st.uniform,st.vonmises,st.vonmises_line,st.wald,st.weibull_min,st.weibull_max,st.wrapcauchy
    ]

    # Best holders
    best_distribution = st.norm
    best_params = (0.0, 1.0)
    best_sse = np.inf

    # Estimate distribution parameters from data
    for distribution in DISTRIBUTIONS:

        # Try to fit the distribution
        try:
            # Ignore warnings from data that can't be fit
            with warnings.catch_warnings():
                warnings.filterwarnings('ignore')

                # fit dist to data
                params = distribution.fit(data)

                # Separate parts of parameters
                arg = params[:-2]
                loc = params[-2]
                scale = params[-1]

                # Calculate fitted PDF and error with fit in distribution
                pdf = distribution.pdf(x, loc=loc, scale=scale, *arg)
                sse = np.sum(np.power(y - pdf, 2.0))

                # if axis pass in add to plot
                try:
                    if ax:
                        pd.Series(pdf, x).plot(ax=ax)
                except Exception:
                    pass

                # identify if this distribution is better
                if best_sse > sse > 0:
                    best_distribution = distribution
                    best_params = params
                    best_sse = sse

        except Exception:
            pass

    return (best_distribution.name, best_params)


df_loan['addr_state'].unique()
#
# # # Make a list with each of the regions by state.
#
west = ['CA', 'OR', 'UT', 'WA', 'CO', 'NV', 'AK', 'MT', 'HI', 'WY', 'ID']
south_west = ['AZ', 'TX', 'NM', 'OK']
south_east = ['GA', 'NC', 'VA', 'FL', 'KY', 'SC', 'LA', 'AL', 'WV', 'DC', 'AR', 'DE', 'MS', 'TN']
mid_west = ['IL', 'MO', 'MN', 'OH', 'WI', 'KS', 'MI', 'SD', 'IA', 'NE', 'IN', 'ND']
north_east = ['CT', 'NY', 'PA', 'NJ', 'RI', 'MA', 'MD', 'VT', 'NH', 'ME']

df_loan['region'] = np.nan


def finding_regions(state):
    if state in west:
        return 'West'
    elif state in south_west:
        return 'SouthWest'
    elif state in south_east:
        return 'SouthEast'
    elif state in mid_west:
        return 'MidWest'
    elif state in north_east:
        return 'NorthEast'


df_loan['region'] = df_loan['addr_state'].apply(finding_regions)
#
plt.style.use('dark_background')
cmap = plt.cm.Set3

by_issued_amount = df_loan.groupby(['issue_d', 'region']).funded_amnt.sum()
by_issued_amount.unstack().plot(stacked=False, colormap=cmap, grid=False, legend=True, figsize=(15,6))

plt.title('Loans issued by Region', fontsize=16)

region = ['West', 'SouthWest', 'SouthEast', 'MidWest', 'NorthEast']
for item in region:
    df_region = df_loan.loc[df_loan['region'] == item]
    totals_grades = list()
    for g in grades:
        totals_grades.append(df_region.loc[df_region['grade'] == g]['grade'].count())
    plt.clf()
    plt.cla()
    plt.pie(totals_grades, labels=grades)
    plt.savefig(f'{item}pie_grades.png')

df_loan.groupby("term").agg({"int_rate":np.mean, "funded_amnt": np.mean}, axis=1).reset_index()

plt.scatter(df_loan['year'], df_loan['int_rate'])


df_loan['income_category'] = np.nan
lst = [df_loan]

for col in lst:
    col.loc[col['annual_inc'] <= 100000, 'income_category'] = 'Low'
    col.loc[(col['annual_inc'] > 100000) & (col['annual_inc'] <= 200000), 'income_category'] = 'Medium'
    col.loc[col['annual_inc'] > 200000, 'income_category'] = 'High'
#
employment_length = ['10+ years', '< 1 year', '1 year', '3 years', '8 years', '9 years',
                    '4 years', '5 years', '6 years', '2 years', '7 years', 'n/a']
df_loan['emp_length_int'] = np.nan

for col in lst:
    col.loc[col['emp_length'] == '10+ years', "emp_length_int"] = 10
    col.loc[col['emp_length'] == '9 years', "emp_length_int"] = 9
    col.loc[col['emp_length'] == '8 years', "emp_length_int"] = 8
    col.loc[col['emp_length'] == '7 years', "emp_length_int"] = 7
    col.loc[col['emp_length'] == '6 years', "emp_length_int"] = 6
    col.loc[col['emp_length'] == '5 years', "emp_length_int"] = 5
    col.loc[col['emp_length'] == '4 years', "emp_length_int"] = 4
    col.loc[col['emp_length'] == '3 years', "emp_length_int"] = 3
    col.loc[col['emp_length'] == '2 years', "emp_length_int"] = 2
    col.loc[col['emp_length'] == '1 year', "emp_length_int"] = 1
    col.loc[col['emp_length'] == '< 1 year', "emp_length_int"] = 0.5
    col.loc[col['emp_length'] == 'n/a', "emp_length_int"] = 0

# sns.boxplot(x="income_category", y="emp_length_int", data=df_loan, palette="Set2")
# sns.boxplot(x="income_category", y="interest_rate", data=df_loan, palette="Set2")
#
bad_loan = ["Charged Off", "Default", "Does not meet the credit policy. Status:Charged Off", "In Grace Period",
            "Late (16-30 days)", "Late (31-120 days)"]

df_loan['loan_condition'] = np.nan


def loan_condition(status):
    if status in bad_loan:
        return 'Bad Loan'
    else:
        return 'Good Loan'


df_loan['loan_condition'] = df_loan['loan_status'].apply(loan_condition)

loan_con = ['Bad Loan', 'Good Loan']
totals_lon_con = list()
for item in loan_con:
    totals_lon_con.append(df_loan.loc[df_loan['loan_condition'] == item]['loan_condition'].count())

plt.pie(totals_lon_con, explode=[0, 0.25], labels=loan_con, autopct='%1.0f%%', pctdistance=0.5, labeldistance=1.2,
        colors=['#FF0000', '#008000'])

palette = ["#3791D7", "#E01E1B"]

sns.barplot(x="year", y="funded_amnt", hue="loan_condition", data=df_loan, palette=palette, estimator=lambda x: len(x)/len(df_loan) * 100)

sns.barplot(x="region", y="int_rate", hue="loan_condition", data=df_loan, palette=palette, estimator=lambda x: len(x)/len(df_loan) * 100)

sns.barplot(x="region", y="funded_amnt", hue="loan_condition", data=df_loan, palette=palette, estimator=lambda x: len(x)/len(df_loan) * 100)

sns.barplot(x="purpose", y="funded_amnt", hue="loan_condition", data=df_loan, palette=palette, estimator=lambda x: len(x)/len(df_loan) * 100)

sns.barplot(x="term", y="funded_amnt", hue="loan_condition", data=df_loan, palette=palette, estimator=lambda x: len(x)/len(df_loan) * 100)

sns.barplot(x="term", y="int_rate", hue="loan_condition", data=df_loan, palette=palette, estimator=lambda x: len(x)/len(df_loan) * 100)


f, ((ax1, ax2)) = plt.subplots(1, 2)
cmap = plt.cm.coolwarm

by_credit_score = df_loan.groupby(['year', 'grade']).funded_amnt.mean()
by_credit_score.unstack().plot(legend=False, ax=ax1, figsize=(14, 4), colormap=cmap)
ax1.set_title('Loans issued by Credit Score', fontsize=14)

by_inc = df_loan.groupby(['year', 'grade']).interest_rate.mean()
by_inc.unstack().plot(ax=ax2, figsize=(14, 4), colormap=cmap)
ax2.set_title('Interest Rates by Credit Score', fontsize=14)

ax2.legend(bbox_to_anchor=(-1.0, -0.3, 1.7, 0.1), loc=5, prop={'size': 12},
           ncol=7, mode="expand", borderaxespad=0.)

fig = plt.figure(figsize=(16,12))

ax1 = fig.add_subplot(221)
ax2 = fig.add_subplot(222)
ax3 = fig.add_subplot(212)

cmap = plt.cm.coolwarm_r

loans_by_region = df_loan.groupby(['grade', 'loan_condition']).size()
loans_by_region.unstack().plot(kind='bar', stacked=True, colormap=cmap, ax=ax1, grid=False)
ax1.set_title('Type of Loans by Grade', fontsize=14)


loans_by_grade = df_loan.groupby(['sub_grade', 'loan_condition']).size()
loans_by_grade.unstack().plot(kind='bar', stacked=True, colormap=cmap, ax=ax2, grid=False)
ax2.set_title('Type of Loans by Sub-Grade', fontsize=14)

by_interest = df_loan.groupby(['year', 'loan_condition']).int_rate.mean()
by_interest.unstack().plot(ax=ax3, colormap=cmap)
ax3.set_title('Average Interest rate by Loan Condition', fontsize=14)
ax3.set_ylabel('Interest Rate (%)', fontsize=12)


plt.figure(figsize=(18,18))

# # Create a dataframe for bad loans
bad_df = df_loan.loc[df_loan['loan_condition'] == 'Bad Loan']

plt.subplot(211)
g = sns.boxplot(x='home_ownership', y='funded_amnt', hue='loan_condition',
               data=bad_df, color='r')

g.set_xticklabels(g.get_xticklabels(), rotation=45)
g.set_xlabel("Type of Home Ownership", fontsize=12)
g.set_ylabel("Loan Amount", fontsize=12)
g.set_title("Distribution of Amount Borrowed \n by Home Ownership", fontsize=16)



plt.subplot(212)
g1 = sns.boxplot(x='year', y='loan_amount', hue='home_ownership',
               data=bad_df, palette="Set3")
g1.set_xticklabels(g1.get_xticklabels(),rotation=45)
g1.set_xlabel("Type of Home Ownership", fontsize=12)
g1.set_ylabel("Loan Amount", fontsize=12)
g1.set_title("Distribution of Amount Borrowed \n through the years", fontsize=16)


plt.subplots_adjust(hspace = 0.6, top = 0.8)

# predicting good loans vs bad loans without under sampling
tree = DecisionTreeClassifier()
random_forrest = RandomForestClassifier()
logistic_reg = LogisticRegression()
df_loan = df_loan.dropna(axis=0)
X = df_loan[['term', 'funded_amnt', 'int_rate', 'sub_grade', 'emp_length_int', 'home_ownership',
 'income_category', 'region', 'revol_bal']]
X = pd.get_dummies(X)
y = df_loan['loan_condition']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)
tree.fit(X_train, y_train)
logistic_reg.fit(X_train, y_train)
random_forrest.fit(X_train, y_train)
print(tree.score(X_test, y_test))
print(logistic_reg.score(X_test, y_test))
print(random_forrest.score(X_test, y_test))

def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()
#
y_pred = tree.predict(X_test)
cnf_matrix = confusion_matrix(y_test, y_pred)
np.set_printoptions(precision=2)

# Plot non-normalized confusion matrix
plt.figure()
plot_confusion_matrix(cnf_matrix, classes=['Bad Loan', 'Good Loan'],
                      title='Confusion matrix, without normalization, dt')

plt.show()

y_pred = random_forrest.predict(X_test)
cnf_matrix = confusion_matrix(y_test, y_pred)
np.set_printoptions(precision=2)

# # Plot non-normalized confusion matrix
plt.figure()
plot_confusion_matrix(cnf_matrix, classes=['Bad Loan', 'Good Loan'],
                      title='Confusion matrix, without normalization, random forrest')

plt.show()

y_pred = logistic_reg.predict(X_test)
cnf_matrix = confusion_matrix(y_test, y_pred)
np.set_printoptions(precision=2)

# Plot non-normalized confusion matrix
plt.figure()
plot_confusion_matrix(cnf_matrix, classes=['Bad Loan', 'Good Loan'],
                      title='Confusion matrix, without normalization, logreg')
plt.show()

# # Under Sampling
bad_loans = len(df_loan[df_loan['loan_condition'] == 'Bad Loan'])

good_loan_indices = df_loan[df_loan.loan_condition == 'Good Loan'].index

random_indices = np.random.choice(good_loan_indices,bad_loans, replace=False)

bad_loans_indices = df_loan[df_loan.loan_condition == 'Bad Loan'].index

under_sample_indices = np.concatenate([bad_loans_indices,random_indices])

under_sample = df_loan.loc[under_sample_indices]

under_sample = under_sample.dropna(axis=0)

# # Predicting Good loan and bad loans
X = under_sample[['term', 'funded_amnt', 'int_rate', 'sub_grade', 'emp_length_int', 'home_ownership',
 'income_category', 'region', 'revol_bal']]
X = pd.get_dummies(X)
y = under_sample['loan_condition']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)

svm_classifier = svm.SVC()
svm_classifier.fit (X_train, y_train)
print(svm_classifier.score(X_test, y_test))
tree = DecisionTreeClassifier()
random_forrest = RandomForestClassifier()
logistic_reg = LogisticRegression()

tree.fit(X_train, y_train)
logistic_reg.fit(X_train, y_train)
random_forrest.fit(X_train, y_train)
print(tree.score(X_test, y_test))
print(logistic_reg.score(X_test, y_test))
print(random_forrest.score(X_test, y_test))


y_pred = tree.predict(X_test)
cnf_matrix = confusion_matrix(y_test, y_pred)
np.set_printoptions(precision=2)

# Plot non-normalized confusion matrix
plt.figure()
plot_confusion_matrix(cnf_matrix, classes=['Bad Loan', 'Good Loan'],
                      title='Confusion matrix, without normalization, dt')

plt.show()

y_pred = random_forrest.predict(X_test)
cnf_matrix = confusion_matrix(y_test, y_pred)
np.set_printoptions(precision=2)

# Plot non-normalized confusion matrix
plt.figure()
plot_confusion_matrix(cnf_matrix, classes=['Bad Loan', 'Good Loan'],
                      title='Confusion matrix, without normalization, random forrest')

plt.show()

y_pred = logistic_reg.predict(X_test)
cnf_matrix = confusion_matrix(y_test, y_pred)
np.set_printoptions(precision=2)

# Plot non-normalized confusion matrix
plt.figure()
plot_confusion_matrix(cnf_matrix, classes=['Bad Loan', 'Good Loan'],
                      title='Confusion matrix, without normalization, logreg')
plt.show()

dot_data = StringIO()
export_graphviz(tree, out_file=dot_data,
                filled=True, rounded=True,
                special_characters=True)

linear = LinearRegression()
df_loan.dropna(axis=0)
X = df_loan[['grade', 'annual_inc']]
X = pd.get_dummies(X)
y = df_loan['int_rate']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)
X2 = sm.add_constant(X_train)
est = sm.OLS(y_train, X2)
est2 = est.fit()
print(est2.summary())
linear.fit(X_train, y_train)

# grade prediction

def finding_grades(state):
    if state == 'A':
        return 1
    elif state == 'B':
        return 2
    elif state == 'C':
        return 3
    elif state == 'D':
        return 4
    elif state == 'E':
        return 5
    elif state == 'F':
        return 6
    elif state == 'G':
        return 7
#
log_reg = LogisticRegression()
X = df_loan[['term', 'dti', 'purpose']]
X = pd.get_dummies(X)
y = df_loan['grade']
y = pd.get_dummies(y)
logit = sm.Logit(y['B'], X)
result = logit.fit()
print(result.summary())
X = df_loan[['int_rate', 'loan_amount', 'term', 'emp_length_int', 'home_ownership']]
X = pd.get_dummies(X)
kmeans = KMeans(n_clusters=20)
kmeans.fit(X)
b = kmeans.labels_

