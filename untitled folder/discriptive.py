import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split

from sklearn.ensemble import RandomForestClassifier
from sklearn import preprocessing
from sklearn.metrics import confusion_matrix, accuracy_score

df_sales = pd.read_csv('train.csv')
df_stores = pd.read_csv('store_antiquety.csv')
i = 1
while i < 1116:
    globals()['df%s' % i] = df_sales.loc[df_sales['Store'].isin([f'{i}'])].loc[df_sales['Open'] != 0]
    i += 1

i = 0
means_list = np.zeros((1115, 2))
while i < 1115:
    means_list[i][0] = globals()['df%s' % str(i+1)]['Sales'].mean()
    means_list[i][1] = globals()['df%s' % str(i+1)]['Customers'].mean()
    i += 1
means = pd.DataFrame(means_list, columns=['Sale_mean', 'Customer_mean'])


means_list = np.zeros((1115, 4))
i = 0
while i < 1115:
    means_list[i][0] = globals()['df%s' % str(i+1)].loc[globals()['df%s' % str(i+1)]['StateHoliday'] != '0']['Sales'].mean()
    means_list[i][1] = globals()['df%s' % str(i+1)].loc[globals()['df%s' % str(i+1)]['StateHoliday'] != '0']['Customers'].mean()
    means_list[i][2] = globals()['df%s' % str(i+1)].loc[globals()['df%s' % str(i+1)]['StateHoliday'] == '0']['Sales'].mean()
    means_list[i][3] = globals()['df%s' % str(i+1)].loc[globals()['df%s' % str(i+1)]['StateHoliday'] == '0']['Customers'].mean()
    i += 1

means_state_holiday = pd.DataFrame(means_list, columns=['Sale_mean_state_holiday',
                                                        'Customer_mean_state_holiday',
                                                        'Sale_mean_not_state_holiday',
                                                        'Customer_mean_not_state_holiday'])

means_list = np.zeros((1115, 4))
i = 0
while i < 1115:
    means_list[i][0] = globals()['df%s' % str(i+1)].loc[globals()['df%s' % str(i+1)]['SchoolHoliday'] != 0]['Sales'].mean()
    means_list[i][1] = globals()['df%s' % str(i+1)].loc[globals()['df%s' % str(i+1)]['SchoolHoliday'] != 0]['Customers'].mean()
    means_list[i][2] = globals()['df%s' % str(i+1)].loc[globals()['df%s' % str(i+1)]['SchoolHoliday'] == 0]['Sales'].mean()
    means_list[i][3] = globals()['df%s' % str(i+1)].loc[globals()['df%s' % str(i+1)]['SchoolHoliday'] == 0]['Customers'].mean()
    i += 1


means_school_holiday = pd.DataFrame(means_list, columns=['Sale_mean_SchoolHoliday',
                                                         'Customer_mean_SchoolHoliday',
                                                         'Sale_mean_not_SchoolHoliday',
                                                         'Customer_mean_not_sSchoolHoliday'])
means_list = np.zeros((1115, 3))
i = 0
while i < 1115:
    means_list[i][0] = i+1
    means_list[i][1] = globals()['df%s' % str(i+1)].loc[globals()['df%s' % str(i+1)]['Promo'] == 1]['Sales'].mean()
    means_list[i][2] = globals()['df%s' % str(i+1)].loc[globals()['df%s' % str(i+1)]['Promo'] == 1]['Sales'].mean()
    i += 1

means_Promo_no_Promo = pd.DataFrame(means_list, columns=['Store',
                                                         'Sale_mean_Promo',
                                                         'Sale_mean_no_Promo', ])

# df_stores_all = df_stores.join(means)
# df_stores_all = df_stores_all.join(means_state_holiday)
# df_stores_all = df_stores_all.join(means_school_holiday)
#
df_cluster = df_stores.drop(['Store'], axis=1).drop(['CompetitionOpenSinceMonth'], axis=1).drop(['CompetitionOpenSinceYear'], axis=1).drop(['Promo2SinceWeek'], axis=1).drop(['Promo2SinceYear'], axis=1)
# .drop(['Assortment'], axis=1).drop(['StoreType'], axis=1).drop(['antiquity'], axis=1)

# # kmeans = KMeans(n_clusters=4)
# # kmeans.fit(df_cluster)
# # b = kmeans.labels_
#
# tree = RandomForestClassifier()
# lab_enc = preprocessing.LabelEncoder()
# x = df_cluster
y = means.drop(['Customer_mean'], axis=1)
#
bins = [720, 2330, 6910, 20000, 80000]
a = np.zeros(1115)
p = pd.DataFrame(a, columns=['group'])
l = list(range(4))

p['group'] = pd.cut(y.Sale_mean, bins=bins, labels=l)
# y = lab_enc.fit_transform(np.array(p).flatten())
#
# X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.33)
# tree.fit(X_train, y_train)
#
# y_2 = tree.predict(X_test)
#
# print(tree.score(X_test, y_test))
# # #
# # print(confusion_matrix(y, y_2).ravel())
